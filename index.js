let ashKetchum = {
	name: "Ash Ketchum",
	age: 10,
	pokemon: ["Pikachu", "Charizard", "Squirtle", "Bulbasaur"],
	friends: {
		hoenn: ["May","Max"],
		kanto: ["Brock", "Misty"]
	},
	talkMethod: function (){
		console.log("Pikachu! I choose you!");
	}
}

console.log(ashKetchum)
console.log("Result of dot notation:")
console.log(ashKetchum.name)
console.log("Result of square bracket notation")
console.log(ashKetchum.pokemon)
console.log("Result of talk method")
ashKetchum.talkMethod()

function Pokemon(name, level) {
	// properties
	this.name = name
	this.level = level;
	this.health = 2*level;
	this.attack = level;

	// methods
	this.tackle = function(target){
		console.log(this.name + ' tackled ' + target.name)
		console.log(target.name + "'s health is now reduced to " + Number(target.health - this.attack));
		target.health = Number(target.health - this.attack)
		console.log(target)
	let currentHealth = Number(target.health - this.attack)
	if (currentHealth < 1){
			console.log(target.name + " fainted.")}
	}
}

let pikachu = new Pokemon("Pikachu", 12)
console.log(pikachu)

let geodude = new Pokemon("Geodude", 8)
console.log(geodude)

let mewtwo = new Pokemon("Mewtwo", 100)
console.log(mewtwo)


geodude.tackle(pikachu)

mewtwo.tackle(geodude)

pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)
pikachu.tackle(mewtwo)




